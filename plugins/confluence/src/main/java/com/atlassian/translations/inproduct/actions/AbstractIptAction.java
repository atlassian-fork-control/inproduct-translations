package com.atlassian.translations.inproduct.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.xwork.SimpleXsrfTokenGenerator;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * User: kalamon
 * Date: 26.06.13
 * Time: 13:29
 */
public abstract class AbstractIptAction extends ConfluenceActionSupport {
    private static final ResourceBundle bundle = ResourceBundle.getBundle("i18n.admini18n");
    protected final WebResourceManager wrm;

    protected AbstractIptAction(WebResourceManager webResourceManager) {
        wrm = webResourceManager;
    }

    public String getRequestParameter(final String name) {
        @SuppressWarnings("unchecked")
        final Map<String, String[]> requestParameters = ActionContext.getContext().getParameters();
        final String[] parameter = requestParameters.get(name);
        if (parameter == null || parameter.length == 0) {
            return null;
        }
        return parameter[0];
    }

    @Override
    @HtmlSafe
    public String getText(String key) {
        try {
            return bundle.getString(key);
        } catch (MissingResourceException e) {
            return super.getText(key);
        }
    }

    public boolean hasPermissions() {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION);
    }

    public String getProduct() {
        return "Confluence";
    }

    public String getAtl_token() {
        return new SimpleXsrfTokenGenerator().generateToken(ServletActionContext.getRequest());
    }

    protected boolean xsrfOk() {
        return new SimpleXsrfTokenGenerator().validateToken(ServletActionContext.getRequest(), getRequestParameter("atl_token"));
    }

    @Override
    public String execute() throws Exception {
        wrm.requireResource("com.atlassian.translations.confluence.inproduct:admin-css");
        boolean allowed = hasPermissions();
        return allowed ? SUCCESS : ERROR;
    }
}
