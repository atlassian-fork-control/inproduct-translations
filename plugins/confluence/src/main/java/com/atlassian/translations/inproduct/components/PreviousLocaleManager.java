package com.atlassian.translations.inproduct.components;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;

import java.util.Locale;

/**
 *
 *
 * @since v5.0
 */
public class PreviousLocaleManager {

    private final LocaleManager localeManager;
    Locale previousLocale;

    public PreviousLocaleManager(LocaleManager localeManager) {
        this.localeManager = localeManager;
    }

    /**
	 * @return true if locale has changed since last call
	 */
	public boolean hasLocaleChanged() {

        Locale locale = localeManager.getLocale(AuthenticatedUserThreadLocal.get());

		// cannot determine locale
		if (locale == null) {
			return false;
		}

		// the same locale
		if (previousLocale != null && previousLocale.equals(locale)) {
			return false;
		}

		previousLocale = locale;

		return true;
	}
}
