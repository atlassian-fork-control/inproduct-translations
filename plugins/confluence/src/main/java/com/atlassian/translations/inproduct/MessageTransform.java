package com.atlassian.translations.inproduct;

import com.atlassian.confluence.languages.TranslationTransform;
import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.translations.inproduct.components.MessageKeyGuesser;
import com.atlassian.translations.inproduct.components.TranslationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
public class MessageTransform extends AbstractMessageTransform implements TranslationTransform {
	public MessageTransform(TranslationManager translationManager, MessageKeyGuesser guesser) {
        super(translationManager, guesser);
	}

    @Override
    protected HttpServletRequest getCurrentRequest() {
        return ServletContextThreadLocal.getRequest();
    }
}
