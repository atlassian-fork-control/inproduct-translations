package com.atlassian.translations.inproduct;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.LookAndFeelBean;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import org.apache.log4j.Logger;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 14:57
 */
public class JiraMessageTransformUtil {
    private static final Logger LOG = Logger.getLogger(JiraMessageTransformUtil.class);

    //
    // This is an evil hack to make plugin framework flush transformed resource cache.
    // It could break at any moment, so beware.
    //
    public static void clearWebResourceCache(
            PluginResourceLocator pluginResourceLocator, EventPublisher eventPublisher, PluginAccessor pluginAccessor) {
        try {
            // this fails
//            ((PluginResourceLocatorImpl) pluginResourceLocator).onPluginEnabled(null);

            // maybe this?
            Plugin plugin = pluginAccessor.getEnabledPlugin("com.atlassian.translations.jira.inproduct");
            eventPublisher.publish(new PluginEnabledEvent(plugin));

            // this is supposed to make the browser reject its cache. Will it work?
            ApplicationProperties ap = ComponentManager.getComponent(ApplicationProperties.class);
            LookAndFeelBean lookAndFeelBean = LookAndFeelBean.getInstance(ap);
            lookAndFeelBean.updateVersion(lookAndFeelBean.getVersion() + 1);
        } catch (Exception e) {
            LOG.error(e);
        }
    }

}
