package com.atlassian.translations.inproduct.conditions;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: kalamon
 * Date: 30.07.13
 * Time: 10:01
 */
public class IsPriorToJiraVersion implements Condition {

    private int maxMajorVersion;
    private int maxMinorVersion;
    private int majorVersion;
    private int minorVersion;

    public IsPriorToJiraVersion() {
        BuildUtilsInfo bui = ComponentManager.getComponentInstanceOfType(BuildUtilsInfo.class);
        String versionString = bui.getVersion();
        String versionRegex = "^(\\d+)\\.(\\d+)";
        Pattern versionPattern = Pattern.compile(versionRegex);
        Matcher versionMatcher = versionPattern.matcher(versionString);
        versionMatcher.find();
        majorVersion = Integer.decode(versionMatcher.group(1));
        minorVersion = Integer.decode(versionMatcher.group(2));
    }

    public void init(final Map<String, String> paramMap) throws PluginParseException {
        maxMajorVersion = Integer.decode(paramMap.get("majorVersion"));
        maxMinorVersion = Integer.decode(paramMap.get("minorVersion"));
    }

    public boolean shouldDisplay(final Map<String, Object> context) {
        return shouldDisplay(maxMajorVersion, maxMinorVersion);
    }

    public boolean shouldDisplay(int maxMaj, int maxMin) {
        return (majorVersion < maxMaj) || (majorVersion == maxMaj) && (minorVersion < maxMin);
    }
}