package com.atlassian.translations.inproduct.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.translations.inproduct.JiraIptUtils;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:02
 */
public class AoTranslationsManager extends AbstractAoTranslationsManager {
    public AoTranslationsManager(ActiveObjects ao) {
        super(ao);
    }

    @Override
    protected String getText(String key) {
        return JiraIptUtils.getI18nHelper().getText(key);
    }
}
