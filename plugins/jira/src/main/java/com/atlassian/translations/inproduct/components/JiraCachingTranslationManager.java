package com.atlassian.translations.inproduct.components;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.translations.inproduct.ao.AoTranslationsManager;

import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * User: kalamon
 * Date: 10.07.12
 * Time: 15:08
 */
public class JiraCachingTranslationManager extends AbstractCachingTranslationManager {
    private final TaskManager taskManager;

    public JiraCachingTranslationManager(ActiveObjects ao, TaskManager taskManager) {
        super(new AoTranslationsManager(ao));
        this.taskManager = taskManager;
    }

    @Override
    protected void runTask(Callable<Serializable> task, String message) {
        taskManager.submitTask(task, message, new TaskContext() {
            @Override
            public String buildProgressURL(Long aLong) {
                return null;
            }
        });
    }
}
