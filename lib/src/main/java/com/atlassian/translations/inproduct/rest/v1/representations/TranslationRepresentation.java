package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class TranslationRepresentation {
	@XmlElement
	private String translation;

	public TranslationRepresentation() {
	}

	public TranslationRepresentation(String translation) {
		this.translation = translation;
	}
}
