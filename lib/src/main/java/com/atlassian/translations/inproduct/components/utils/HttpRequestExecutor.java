package com.atlassian.translations.inproduct.components.utils;

import org.apache.http.HttpHost;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpRequest;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpPost;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class HttpRequestExecutor {
    private static final String LOGIN_TOKEN_NAME = "__ATL_TOKEN";
    private static final String cookieDomain = ".atlassian.com";

    public String executeRequest(HttpHost host, HttpPost req, String loginToken) throws IOException {
        DefaultHttpClient client = new DefaultHttpClient();
        if (loginToken != null) {
            addLoginToken(client, req, loginToken);
        }

        String result;
        try {
            HttpResponse response = client.execute(host, req);
            InputStream stream = response.getEntity().getContent();
            result = IOUtils.toString(stream, "UTF-8");
            stream.close();
            EntityUtils.consume(response.getEntity());

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new HttpResponseException(response.getStatusLine().getStatusCode(), result);
            }
        } finally {
            req.releaseConnection();
        }

        return result;
    }

    private void addLoginToken(DefaultHttpClient client, HttpRequest request, String loginToken) {
        //Use CookiePolicy.BROWSER_COMPATIBILITY while working with dev/staging
        request.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2965);
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie(LOGIN_TOKEN_NAME, loginToken);
        cookie.setDomain(cookieDomain);
        cookieStore.addCookie(cookie);
        client.setCookieStore(cookieStore);
    }
}
