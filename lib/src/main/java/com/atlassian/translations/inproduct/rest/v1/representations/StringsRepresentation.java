package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: kalamon
 * Date: 02.07.12
 * Time: 15:28
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StringsRepresentation {
    @XmlElement private List<I18NStringRepresentation> strings = new ArrayList<I18NStringRepresentation>();

    public StringsRepresentation(List<I18NStringRepresentation> strings) {
        this.strings = strings;
    }

    public StringsRepresentation() {
    }
}
